#!/bin/bash

JTA_ENGINE_PATH=/home/jenkins
JTA_FRONTEND_PATH=/var/lib/jenkins

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [ "$1" = "update" ]
then
    echo "Updating frontend and backend"
    set -x
    /etc/init.d/jenkins stop
    cp -ar jta/engine/* $JTA_ENGINE_PATH/
    cp -ar jta/jenkins/* $JTA_FRONTEND_PATH/
    cp conf/jenkins.cfg /etc/default/jenkins
    cp conf/config.xml $JTA_FRONTEND_PATH/config.xml

    # fill in JTA_ENGINE_PATH_PLACEHOLDER with real value in all jenkins jobs
    find $JTA_FRONTEND_PATH -type f -exec sed -i "s#JTA_ENGINE_PATH_PLACEHOLDER#$JTA_ENGINE_PATH#" {} +

    # set global JTA_ENGINE_PATH global var in jenkins config
    sed -i "s#JTA_ENGINE_PATH_PLACEHOLDER#$JTA_ENGINE_PATH#" $JTA_FRONTEND_PATH/config.xml

    cp docs/jta-guide.pdf $JTA_ENGINE_PATH/jta-docs.pdf

    chown -R jenkins  $JTA_ENGINE_PATH
    chown -R jenkins  $JTA_FRONTEND_PATH

    /etc/init.d/jenkins stop
    exit
fi

echo Installing prereqs...
echo deb http://ftp.us.debian.org/debian wheezy main non-free >> /etc/apt/sources.list
aptitude update
aptitude -VR install daemon gcc make python-paramiko python-lxml python-simplejson python-matplotlib libtool xmlstarlet autoconf automake rsync openjdk-7-jre openjdk-7-jdk iperf netperf netpipe-tcp texlive-latex-base sshpass

echo "adding multiarch support"
dpkg --add-architecture i386
aptitude update
aptitude install -VR ia32-libs

echo Please select NO in the following dialogue. Press return now.
read
dpkg-reconfigure dash

echo Downloading and installing jenkins pkg...
# wget http://ftp.vim.org/programming/jenkins/debian-stable/jenkins_1.509.2_all.deb
dpkg -i install/jenkins_1.509.2_all.deb

echo Installing stuff ...
mkdir $JTA_ENGINE_PATH
cp -ar jta/engine/* $JTA_ENGINE_PATH/
cp -ar jta/jenkins/* $JTA_FRONTEND_PATH/
cp conf/jenkins.cfg /etc/default/jenkins
cp conf/config.xml $JTA_FRONTEND_PATH/config.xml

# fill in JTA_ENGINE_PATH_PLACEHOLDER with real value in all jenkins jobs
find $JTA_FRONTEND_PATH -type f -exec sed -i "s#JTA_ENGINE_PATH_PLACEHOLDER#$JTA_ENGINE_PATH#" {} +

# set global JTA_ENGINE_PATH global var in jenkins config
sed -i "s#JTA_ENGINE_PATH_PLACEHOLDER#$JTA_ENGINE_PATH#" $JTA_FRONTEND_PATH/config.xml

ln -fns $JTA_ENGINE_PATH/logs $JTA_FRONTEND_PATH/userContent/jta.logs

mkdir $JTA_ENGINE_PATH/work

mkdir $JTA_ENGINE_PATH/docs
cp docs/jta-guide.pdf $JTA_ENGINE_PATH/jta-docs.pdf

wget "http://downloads.sourceforge.net/project/getfo/texml/texml-2.0.2/texml-2.0.2.tar.gz?r=http%3A%2F%2Fsourceforge.net%2Fprojects%2Fgetfo%2F&ts=1398789654&use_mirror=sunet" -O texml.tar.gz
tar xvf texml.tar.gz
cd texml-2.0.2
python setup.py install
cd -


echo "fetching tools"
git clone https://cogentembedded@bitbucket.org/cogentembedded/jta-tools-public.git tools
mkdir $JTA_ENGINE_PATH/tools

tools/osv-eglibc-x86_64-meta-toolchain-cortexa15hf-vfp-neon-toolchain-1.5+snapshot.sh -y -d $JTA_ENGINE_PATH/tools/renesas-arm

tools/osv-eglibc-x86_64-meta-toolchain-core2-32-toolchain-1.5+snapshot.sh -y -d $JTA_ENGINE_PATH/tools/intel-minnow

chown -R jenkins  $JTA_ENGINE_PATH
chown -R jenkins  $JTA_FRONTEND_PATH
chown -R jenkins  /var/cache/jenkins
chown jenkins /etc/default/jenkins

mkdir $JTA_ENGINE_PATH/overlays/work
chown jenkins $JTA_ENGINE_PATH/overlays

mkdir $JTA_ENGINE_PATH/logs/logruns
chown jenkins $JTA_ENGINE_PATH/logs/logruns

cp conf/jenkins.cfg /etc/default/jenkins

echo Startings jenkins...
/etc/init.d/jenkins start

echo Installing custom frontend...
cd install/jenkins-updates
./updates.sh

