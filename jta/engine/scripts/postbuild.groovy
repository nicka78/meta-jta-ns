device = manager.build.getEnvironment(manager.listener)['NODE_NAME']
sdk_str = manager.build.getEnvironment(manager.listener)['PLATFORM']
sdk_ver = "no sdk"
if (sdk_str) {
  sdk_ver = sdk_str.tokenize(' / ')[1]
}

def log_matcher = manager.getLogMatcher(".*[^\"n]JTA error reason: (.*)\$")
if (log_matcher?.matches()) {
    abort_reason = log_matcher.group(1);
	manager.createSummary("error.gif").appendText("${abort_reason}", false, false, false, "red");
	manager.addErrorBadge("${abort_reason}")
}

def fw_matcher = manager.getLogMatcher("^(.*)(Firmware revision: )(.*)\$")

fw_revision = "unknown"
if (fw_matcher?.matches()) {
    fw_revision = fw_matcher.group(3);
    manager.createSummary("help.gif").appendText("Firmware revision ${fw_revision}", false, false, false, "black");
}

// manager.addShortText("${device} / ${fw_revision} (${sdk_str})", "black", "#FFFFFF", "0px", "");
manager.addShortText("${device} / ${fw_revision}", "black", "#FFFFFF", "0px", "");
