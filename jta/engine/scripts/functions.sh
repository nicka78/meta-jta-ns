# Copyright (c) 2014 Cogent Embedded, Inc.

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

# DESCRIPTION
# This script contains core functions of JTA that needed for running tests

# These are supporting functions for test suites building process.
. $WORKSPACE/../scripts/params.sh
. $WORKSPACE/../scripts/common.sh

# Unpacks $tarball_path/$tarball into current directory.
# $1 - optional flag; if set to "nostrip",
#      the leading path components won't be stripped
function unpack {
  [ "$1" = "nostrip" ] && strip_opt= || strip_opt="--strip-components=1"

  case ${tarball/*./} in
    gz|tgz) key=z ;;
    bz2) key=j ;;
    tar) key= ;;
    *) echo "Unknown $tarball file format. Not unpacking."; return;; 
  esac

  tar ${key}xf $TEST_HOME/$tarball $strip_opt 
}

function is_empty {
# $1 - parameter

 if [ -z $1 ]; then
   echo "ERROR: EMPTY PARAMETER"
   exit
 fi
}

function get {
    ov_transport_get "$@"
}

function put {
    ov_transport_put "$@"
}

# These are supporting functions for target command running
# TODO: Add descriptions for parameters in every function
function cmd {
    ov_transport_cmd "$@"
}

function safe_cmd {
# $1 - ?

  ov_rootfs_oom "$@"    

  cmd "$@"
}

function report {
# $1 - remote shell command, $2 - test log file.
# XXX:$2 this parameter could be optional, by default we can use $TESTDIR/$TESTDIR.log

  is_empty $1

  RETCODE=/tmp/$$-${RANDOM}
  if [ -z $2 ]; then
    echo "WARNING: test log file parameter empty, so will use default"
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee $JTA_HOME/jta.$TESTDIR/$TESTDIR.log; exit \$(cat $RETCODE; rm $RETCODE &>/dev/null)"
  else
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee $2; exit \$(cat $RETCODE; rm $RETCODE &>/dev/null)"
  fi
}

function report_append {
# $1 - remote shell command, $2 - test log file.

  is_empty $1

  RETCODE=/tmp/$$-${RANDOM}
  if [ -z $2 ]; then
    echo "WARNING: test log file parameter empty, so will use default"
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee -a $JTA_HOME/jta.$TESTDIR/$TESTDIR.log; exit \$(cat $RETCODE; rm $RETCODE &>/dev/null)"
  else
    safe_cmd "{ $1; echo \$? > $RETCODE; } 2>&1 | tee -a $2; exit \$(cat $RETCODE; rm $RETCODE &>/dev/null)"
  fi
}

function dump_syslogs {
# 1 - tarball template, 2 - before/after

  is_empty $1
  is_empty $2

  # We create /tmp/${2} dir in any case to capture target logs and prevent
  # log dump to $JTA_HOME dir.

  ov_rootfs_logread "$@"
}

function concurrent_check {
  LOCKFILE="$WORKSPACE/$TRIPLET.build.lock" 

  if [ -e ${LOCKFILE} ]; then

    while $(wget -qO- "$(cat ${LOCKFILE})/api/xml?xpath=*/building/text%28%29") && [ ! -e test_suite_ready ]
    do
      sleep 5
    done
  fi

  echo "${BUILD_URL}" > ${LOCKFILE}
}

# Wait for other builds of the same test running in parallel,
# process Rebuild flag, and unpack test sources if necessary.
# Returns 0 if actual build needs to be performed; 1 - otherwise.
# Build scripts must call this function in the beginning.
# $1 is passed directly to unpack().
function pre_build {
  source $WORKSPACE/../scripts/tools.sh

  mkdir -p $TRIPLET && cd $TRIPLET
  if concurrent_check; then
    [ "$Rebuild" = "true" ] && rm -rf *

    if [ ! -e test_suite_ready ]; then
      unpack $1
      return 0
    fi
  fi
  return 1
}

function build {
  pre_build $1 && test_build || return 1
  post_build
}

function post_build {
  true
}

function deploy {
  pre_deploy
  test_deploy
  post_deploy
}

function pre_deploy {
  cd "$WORKSPACE"
  if [ ! -e "$TRIPLET" ]; then
    if build; then 
      echo "Test builded(looks like first time)"
      cd $WORKSPACE/$TRIPLET
    else
      echo -e "Error reason: unable to change dir to $TRIPLET"
    fi
  else
    cd  $TRIPLET
  fi
}

function post_deploy {
  rm -f $LOCKFILE
}

function firmware {
  ov_get_firmware
  export FWVER="$FW"
}

function pre_test {
# $1 - tarball template
# Make sure the target is alive, and prepare workspace for the test

  export SSHPASS=$PASSWORD

  is_empty $1

# Target cleanup flag check
  [ "$Target_Cleanup" = "true" ] && target_cleanup $1 || true

  cmd "true" || abort_job "Cannot connect to $DEVICE via $TRANSPORT"

# It is needed to create directory for test logs and system logs
  mkdir -p $WORKSPACE/../logs/$JOB_NAME/testlogs
  mkdir -p $WORKSPACE/../logs/$JOB_NAME/systemlogs

  # /tmp/${1} is needed to save logs on different partition

# Get target device firmware.
  firmware
  cmd "echo \"Firmware revision:\" $FWVER" || abort_job "Error while ROOTFS_FWVER command execution on target"

# XXX: Sync date/time between target device and framework host
# Also log memory and disk status as well as non-kernel processes,and interrupts

  ov_rootfs_state 

  cmd "rm -rf $JTA_HOME/jta.$1 /tmp/$1; mkdir -p $JTA_HOME/jta.$1 /tmp/jta.$1" || abort_job "Could not create $1 and /tmp/$1 on $DEVICE"

# Log test name
  ov_logger "logger \"Starting test ${JOB_NAME}\""

  dump_syslogs $1 "before"

# flush buffers to physical media and drop filesystem caches to make system load more predictable during test execution
  ov_rootfs_sync

  ov_rootfs_drop_caches

}

function bench_processing {
  firmware
  export DEVICE=$DEVICE
  export GEN_TESTRES_FILE=$GEN_TESTRES_FILE

  echo -e "\n RESULT ANALYSIS \n"

  # Get the test results
  get_testlog $TESTDIR $JTA_HOME/jta.$TESTDIR/$TESTDIR.log
  DATA_FILE=$JTA_ENGINE_PATH/logs/${JOB_NAME}/plot.data
  REF_FILE=$JTA_ENGINE_PATH/tests/${JOB_NAME}/reference.log
  PYTHON_ARGS="-W ignore::DeprecationWarning -W ignore::UserWarning"
  # The first command checks thresholds, and exits with appropriate return code.
  # Jenkins aborts script execution on any failure, but the second command needs to be executed in any case, and after the first one.
  # Therefore, this trick with 'rc' variable is required to always execute both commands, and pass proper status to Jenkins at the same time.
  run_python $PYTHON_ARGS $WORKSPACE/../tests/${JOB_NAME}/parser.py $JOB_NAME $PLATFORM $BUILD_ID $BUILD_NUMBER $FW $PLATFORM $NODE_NAME && rc=0 || rc=1
  run_python $PYTHON_ARGS $WORKSPACE/../scripts/parser/dataload.py $JOB_NAME $DATA_FILE $REF_FILE
  if [ $rc -eq 1 ]; then
    false
  else 
    true
  fi
}

# search in test log for {!JOB_NAME}_FAIL_PATTERN_n fail cases and abort with message {!JOB_NAME}_FAIL_MESSAGE_n if found
# args: $1 - path to test log
function fail_check_cases () {
    upName=`echo "${JOB_NAME^^}"| tr '.' '_'`
    fcname="${upName}"_FAIL_CASE_COUNT

    fcc="${!fcname}"

    if [ -z "$fcc" ]; then
        return 0
    fi

    echo "Going to check $fcc fail cases for $JOB_NAME"

    fcc=`expr $fcc - 1`
    
    for n in `seq 0 $fcc` 
    do
        fpvarname="${upName}"_FAIL_PATTERN_"${n}"
        fpvarmsg="${upName}"_FAIL_MESSAGE_"${n}"
        
        fptemplate="${!fpvarname}"
        fpmessage="${!fpvarmsg}"
        
        if grep -e "$fptemplate" $1 ;
        then
            echo "Located failing message in $1"
            abort_job "Detected fail message: $fpmessage"
        fi
    done
}

function post_test {
  # source generated prolog.sh file since post_test is called separately
  source $JTA_ENGINE_PATH/work/prolog.sh
  export SSHPASS=$PASSWORD

  # re-source params to set correct DEVICE, LOGIN, SSH vars
  source $WORKSPACE/../scripts/params.sh

# $1 - tarball template, $2,$3,$4 - optional process names to kill

  is_empty $1

  ov_rootfs_kill "$@"

# Syslog dump
  dump_syslogs $1 "after"

# Get syslogs
  get /tmp/jta.${1}/*.${BUILD_ID}.* ${WORKSPACE}/../logs/${JOB_NAME}/systemlogs/

# Remove work and log dirs
  cmd "rm -rf $JTA_HOME/jta.$1 /tmp/jta.$1"

# log test completion message.
  cmd "logger \"Test $1 is finished\""

# Syslog comparison
  syslog_cmp $1

  fail_check_cases ${WORKSPACE}/../logs/${JOB_NAME}/testlogs/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.log || true
}

function target_cleanup {
  cmd "rm -rf $JTA_HOME/* /tmp/* $JTA_HOME/.local"
}

function target_reboot {
  ov_rootfs_reboot

  sleep 30 # This magic number required as we need to wait until device reboots
  cmd "true"
  if [ $? ]; then
   true
  else
   false
  fi
}

# $1 - tarball template
function build_cleanup {
 rm -rf ${1}-${PLATFORM}
}

function log_compare {
# 1 - tarball template, 2 - number of results, 3 - Criteria, 4 - n/p (i.e. negative or possitive)

  cd "${WORKSPACE}/../logs/${JOB_NAME}/testlogs"
  LOGFILE="${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.log"
  PARSED_LOGFILE="${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.{4}.log"

  if [ -e $LOGFILE ]; then
    current_count=`cat $LOGFILE | grep -E "${3}" 2>&1 | wc -l`
    if [ $current_count -eq $2 ];then
      cat $LOGFILE | grep -E "${3}" | tee "$PARSED_LOGFILE"
      local TMP_P=`diff -u ${WORKSPACE}/../ref_logs/${JOB_NAME}/${1}_${4}.log "$PARSED_LOGFILE" 2>&1`  
      if [ $? -ne 0 ];then 
        echo -e "\nJTA error reason: Unexpected test log output:\n$TMP_P\n"
        check_create_functional_logrun "test error"
        false
      else
        check_create_functional_logrun "passed"
        true
      fi
    else 
      echo -e "\nJTA error reason: Mismatch in expected ($2) and actual ($current_count) pos/neg ($4) results. (pattern: $3)\n"
      check_create_functional_logrun "failed"
      false
    fi
  else 
    echo -e "\nJTA error reason: 'logs/${JOB_NAME}/testlogs/$LOGFILE' is missing.\n"
    check_create_functional_logrun "test error"
    false
  fi

  cd -
}

function get_testlog {
# $1 - tarball template,  $2 - full path to logfile
# XXX: It will be unified
  if [ -n "$2" ]; then
    get ${2} ${WORKSPACE}/../logs/${JOB_NAME}/testlogs/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.log
  else
    get $JTA_HOME/jta.$1/$1.log ${WORKSPACE}/../logs/${JOB_NAME}/testlogs/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}.log
  fi;
}

function syslog_cmp {
# $1 - TESTDIR
  PREFIX="${WORKSPACE}/../logs/${JOB_NAME}/systemlogs/${NODE_NAME}.${BUILD_ID}.${BUILD_NUMBER}"
  rc=0
  if [ -f ${PREFIX}.before ]; then
    if diff -ua ${PREFIX}.before ${PREFIX}.after | grep -vEf "$WORKSPACE/../scripts/syslog.ignore" | grep -E -e '\.(Bug:|Oops)'; then
      rc=1
    fi
  # else # special case for "reboot" test
    # if grep -vE -e '\.(info|notice|debug|warn)|Boot Reason: Warm' -f "$WORKSPACE/../scripts/syslog.ignore" ${PREFIX}.after; then
    #   rc=1
    # fi
  fi
  [ $rc -eq 1 ] && echo -e "\nJTA error reason: Unexpected syslog messages.\n"
  return $rc
}


# check is variable is set and fail if otherwise
function check_capability () {
    varname=CAP_$1
    if [ -z "${!varname}" ]
    then
        abort_job "CAP_$1 is not defined. Make sure you use correct overlay with required specs for this test/benchmark"
    fi
}

function hd_test_mount_prepare () {
    HD_MOUNT_BLOCKDEV=$1
    HD_MOUNT_POINT=$2

    if [ "$HD_MOUNT_BLOCKDEV" != "ROOT" ] 
    then
        cmd "umount -f $HD_MOUNT_BLOCKDEV || /bin/true"
        cmd "mount $HD_MOUNT_BLOCKDEV $HD_MOUNT_POINT"
    fi

    cmd "mkdir -p $HD_MOUNT_POINT/jta.$TESTDIR"
 
}

function hd_test_clean_umount() {
    HD_MOUNT_BLOCKDEV=$1
    HD_MOUNT_POINT=$2

    cmd "rm -rf $HD_MOUNT_POINT/jta.$TESTDIR"

    if [ "$HD_MOUNT_BLOCKDEV" != "ROOT" ] 
    then
        cmd "umount -f $HD_MOUNT_BLOCKDEV"
    fi
}
