tarball=scrashme.tar.bz2

function test_build {
    patch -p1 -N -s < $TEST_HOME/scrashme-testfix.patch
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || exit 1
}

function test_deploy {
	put scrashme  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	assert_define FUNCTIONAL_SCRASHME_NUM
	assert_define FUNCTIONAL_SCRASHME_MODE
	
	if [ "$FUNCTIONAL_SCRASHME_MODE" = "RANDOM" ]; then
	    mode="random"
	else 
	    if [ "$FUNCTIONAL_SCRASHME_MODE" = "ROTATE" ]; then
		mode="rotate"
	    else 
		if [ "$FUNCTIONAL_SCRASHME_MODE" = "CAPCHECK" ]; then
		    mode="capcheck"
	        fi
	    fi
	fi

	report "cd $JTA_HOME/jta.$TESTDIR; ./scrashme --mode=$mode -N$FUNCTIONAL_SCRASHME_NUM"  
}

. $JTA_ENGINE_PATH/scripts/stress.sh
