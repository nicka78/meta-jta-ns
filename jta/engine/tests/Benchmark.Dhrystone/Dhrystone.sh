tarball=Dhrystone.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/dhry_1.c.patch || return 1
    CFLAGS+=" -DTIME"
    LDFLAGS+=" -lm"
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" && touch test_suite_ready || return 1
}

function test_deploy {
    put dhrystone  $JTA_HOME/jta.$TESTDIR/ || return 1
}

function test_run {
    assert_define BENCHMARK_DHRYSTONE_LOOPS

    report "cd $JTA_HOME/jta.$TESTDIR; ./dhrystone $BENCHMARK_DHRYSTONE_LOOPS"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
