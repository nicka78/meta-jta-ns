tarball=tiobench-0.3.3.tar.gz

function test_build {
    patch -N -s -p1 < $TEST_HOME/tiobench-fix-conflicting-types.patch
    make LINK="$CC" CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS+="${CFLAGS}" && touch test_suite_ready || exit 1
}

function test_deploy {
	put tiotest  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    assert_define BENCHMARK_TIOBENCH_MOUNT_BLOCKDEV
    assert_define BENCHMARK_TIOBENCH_MOUNT_POINT
    assert_define BENCHMARK_TIOBENCH_THREADS
    assert_define BENCHMARK_TIOBENCH_SIZE
    
    hd_test_mount_prepare $BENCHMARK_TIOBENCH_MOUNT_BLOCKDEV $BENCHMARK_TIOBENCH_MOUNT_POINT

    report "cd $JTA_HOME/jta.$TESTDIR; ./tiotest -d $BENCHMARK_TIOBENCH_MOUNT_POINT -t $BENCHMARK_TIOBENCH_THREADS -f $BENCHMARK_TIOBENCH_SIZE –S"  
    report_append "cd $JTA_HOME/jta.$TESTDIR; ./tiotest -d $BENCHMARK_TIOBENCH_MOUNT_POINT -t $BENCHMARK_TIOBENCH_THREADS -f $BENCHMARK_TIOBENCH_SIZE –W"  

    hd_test_clean_umount $BENCHMARK_TIOBENCH_MOUNT_BLOCKDEV $BENCHMARK_TIOBENCH_MOUNT_POINT
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
