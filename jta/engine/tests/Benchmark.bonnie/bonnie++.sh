tarball=bonnie++-1.03e.tar.gz

function test_build {
    ./configure --host=$HOST --build=`uname -m`-linux-gnu;
    make && touch test_suite_ready || exit 1
}

function test_deploy {
	put bonnie++  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    assert_define BENCHMARK_BONNIE_MOUNT_BLOCKDEV
    assert_define BENCHMARK_BONNIE_MOUNT_POINT
    assert_define BENCHMARK_BONNIE_SIZE
    assert_define BENCHMARK_BONNIE_RAM
    
    hd_test_mount_prepare $BENCHMARK_BONNIE_MOUNT_BLOCKDEV $BENCHMARK_BONNIE_MOUNT_POINT
        
    report "cd $JTA_HOME/jta.$TESTDIR; pwd; ls; ./bonnie\+\+ -d $BENCHMARK_BONNIE_MOUNT_POINT/jta.$TESTDIR -s $BENCHMARK_BONNIE_SIZE -u 0:0 -r $BENCHMARK_BONNIE_RAM"
    sleep 60
        
    hd_test_clean_umount $BENCHMARK_BONNIE_MOUNT_BLOCKDEV $BENCHMARK_BONNIE_MOUNT_POINT
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
