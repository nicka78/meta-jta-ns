#!/bin/python

import os, re, sys

sys.path.insert(0, os.environ['JTA_PARSER_PATH']) 
import common as plib
                  
ref_section_pat = "^\[[\w]+.[gle]{2}\]"
cur_search_str = "^(.*)(\w*:)(\ *)([\d]{1,4}.[\d]{1,4})(\ *)([\d]{1,4}.[\d]{1,4})(.*)"

cur_dict = {}
cur_file = open(plib.CUR_LOG,'r')
print "Reading current values from " + plib.CUR_LOG

cur_raw_values = cur_file.readlines()
cur_file.close()
items_number = len(cur_raw_values)

for cur_item in cur_raw_values:
	cur_match = re.match(cur_search_str, cur_item)
	if cur_match:
		cur_dict[cur_match.group(1)] = cur_match.group(4)+" "+cur_match.group(6)

sys.exit(plib.process_data(ref_section_pat, cur_dict, 's', 'Rate, MB/s'))
