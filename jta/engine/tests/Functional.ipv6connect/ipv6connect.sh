tarball=ipv6connect.tar.gz

function test_build {
    make CC="$CC" LD="$LD" && touch test_suite_ready || exit 1
}

function test_deploy {
	put ipv6connect  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR; ./ipv6connect"  
}

function test_processing {
	true
}

. $JTA_ENGINE_PATH/scripts/functional.sh
