tarball=linus_stress.tar.gz

function test_build {
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || exit 1
}

function test_deploy {
	put linus_stress  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	assert_define FUNCTIONAL_LINUS_STRESS_DIRTY_BG
	assert_define FUNCTIONAL_LINUS_STRESS_DIRTY

	report "cd $JTA_HOME/jta.$TESTDIR; cat /proc/sys/vm/dirty_ratio > ./dirty_ratio; cat /proc/sys/vm/dirty_background_ratio > ./dirty_bg; echo $FUNCTIONAL_LINUS_STRESS_DIRTY > /proc/sys/vm/dirty_ratio; echo $FUNCTIONAL_LINUS_STRESS_DIRTY_BG > /proc/sys/vm/dirty_background_ratio; ./linus_stress; cat ./dirty_ratio > /proc/sys/vm/dirty_ratio; cat ./dirty_bg > /proc/sys/vm/dirty_background_ratio"  
}

function test_processing {
	true
}

. $JTA_ENGINE_PATH/scripts/functional.sh
