tarball=ffsb-6.0-rc2.tar.bz2

function test_build {
    ./configure --host=$HOST --build=`uname -m`-linux-gnu CC=$CC AR=$AR RANLIB=$RANLIB CXX=$CXX CPP=$CPP CXXCPP=$CXXCPP CFLAGS="$CFLAGS";
    make && touch test_suite_ready || exit 1
}

function test_deploy {
        assert_define BENCHMARK_FFSB_MOUNT_POINT
	sed -i "s|/mnt/test1|$BENCHMARK_FFSB_MOUNT_POINT/jta.$TESTDIR|g" examples/profile_everything
	put ffsb  $JTA_HOME/jta.$TESTDIR/
	put examples/profile_everything  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    assert_define BENCHMARK_FFSB_MOUNT_BLOCKDEV
    assert_define BENCHMARK_FFSB_MOUNT_POINT
    
    hd_test_mount_prepare $BENCHMARK_FFSB_MOUNT_BLOCKDEV $BENCHMARK_FFSB_MOUNT_POINT

    report "cd $JTA_HOME/jta.$TESTDIR; ./ffsb profile_everything"

    hd_test_clean_umount $BENCHMARK_FFSB_MOUNT_BLOCKDEV $BENCHMARK_FFSB_MOUNT_POINT
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
