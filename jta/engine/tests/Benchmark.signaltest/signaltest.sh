tarball=signaltest.tar.gz

function test_build {
  make CC="$CC" LD="$LD" LDFLAGS="$LDFLAGS" CFLAGS="$CFLAGS" && touch test_suite_ready || exit 1
}

function test_deploy {
	put signaltest  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	assert_define BENCHMARK_SIGNALTEST_LOOPS

	report "cd $JTA_HOME/jta.$TESTDIR; ./signaltest  -l $BENCHMARK_SIGNALTEST_LOOPS -q"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
