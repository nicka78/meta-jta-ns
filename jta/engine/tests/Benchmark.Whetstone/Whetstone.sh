tarball=Whetstone.tar.bz2

function test_build {
  	CFLAGS+=" -DTIME"
 	make CC="$CC" LD="$LD" LDFLAGS="$LDFLAGS" CFLAGS="$CFLAGS" LIBS=" -lm" && touch test_suite_ready || exit 1
}

function test_deploy {
	put whetstone  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	assert_define BENCHMARK_WHETSTONE_LOOPS
	report "cd $JTA_HOME/jta.$TESTDIR && ./whetstone $BENCHMARK_WHETSTONE_LOOPS"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
