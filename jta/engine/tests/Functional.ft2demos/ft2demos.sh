tarball=ft2demos-2.3.6.tar.bz2

function test_build {
    # ft2demos depends on already built freetype2. Moreover,
    # it is expected that freetype2 directory is located on the same
    # level with ft2demos. Hence, nostrip flag.
    # Freetype2

    # A2 variable represents FT2 version
    a2=${tarball/*-}
    # Original Freetype2 package w/o tests.
    ft_tarball=freetype-${a2%.*.*}.tar.bz2
   
    tar jxf "$TEST_HOME/$ft_tarball"
    mv ${ft_tarball%.*.*} freetype2

    cd freetype2
    ./configure --build=`uname -m`-linux-gnu --host=$HOST $OPTIONS CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" --target="$HOST"
    make

    # Freetype2 Demos
   
    cd ../${tarball%.tar*}
    patch -N -s -p0 < $TEST_HOME/ft2demos.Makefile.patch
    export PATH=/usr/local/bin/:$PATH
    SDKROOT=$SDKROOT CC=$CC AR=$AR RANLIB=$RANLIB CXX=$CXX CPP=$CPP CXXCPP=$CXXCPP LD=$LD make && touch ../test_suite_ready || exit 1
}

function test_deploy {
    put ${tarball%.tar*}/bin/.libs/*  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    report "cd $JTA_HOME/jta.$TESTDIR; ls -1 /usr/share/fonts/truetype/* | xargs -n1 ./ftdump $1"  
}


function test_processing {
    log_compare "$TESTDIR" "12" ".*family:" "p"
    log_compare "$TESTDIR" "0" "fail|error|FAIL|ERROR" "n"
}

source $JTA_ENGINE_PATH/scripts/overlays.sh
set_overlay_vars

source $JTA_ENGINE_PATH/scripts/reports.sh
source $JTA_ENGINE_PATH/scripts/functions.sh

pre_test $TESTDIR

if $Rebuild; then
    build nostrip
fi

deploy

test_run

get_testlog $TESTDIR

test_processing
