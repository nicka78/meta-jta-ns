#tarball=fio-1.58.tar.gz
tarball=fio-2.0.8.tar.gz

function test_build {
#    patch -p1 -N -s < $TEST_HOME/fio-1.58.patch
    patch -p1 -N -s < $TEST_HOME/fio-2.0.8.patch

# >/dev/null

    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP"
    touch ../test_suite_ready
}

function test_deploy {
    assert_define BENCHMARK_FIO_MOUNT_BLOCKDEV
    assert_define BENCHMARK_FIO_MOUNT_POINT

    cd examples
    cp fsx fsx-current
    cp ssd-test ssd-test-current

    # libaio is not available in XXX; O_DIRECT is not supported by XXXfs.
    sed -i -e "s/libaio/posixaio/g" -e "/direct=/d" fsx-current ssd-test-current
    sed -i -e "s|mount\-point\-of\-ssd|$BENCHMARK_FIO_MOUNT_POINT/jta.$TESTDIR|g" -e "s/1g/90m/g" ssd-test-current

    # Decrease test execution time by an order of magnitude
    sed -i -e "/loops=/cloops=10000" fsx-current
    echo "directory=$BENCHMARK_FIO_MOUNT_POINT/jta.$TESTDIR" >> fsx-current
    cd -

    put fio examples/fsx-current examples/ssd-test-current  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    assert_define BENCHMARK_FIO_MOUNT_BLOCKDEV
    assert_define BENCHMARK_FIO_MOUNT_POINT
    assert_define BENCHMARK_FIO_TIMEOUT
    
        hd_test_mount_prepare $BENCHMARK_FIO_MOUNT_BLOCKDEV $BENCHMARK_FIO_MOUNT_POINT

	report "cd $JTA_HOME/jta.$TESTDIR; ./fio ssd-test-current --timeout=$timeout"  
	report_append "cd $JTA_HOME/jta.$TESTDIR; ./fio fsx-current --timeout=$BENCHMARK_FIO_TIMEOUT"
        
        hd_test_clean_umount $BENCHMARK_FIO_MOUNT_BLOCKDEV $BENCHMARK_FIO_MOUNT_POINT
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
