tarball=ebizzy-0.3.tar.gz

function test_build {
    $CC -Wall -Wshadow -lpthread  -o ebizzy ebizzy.c && touch test_suite_ready || exit 1
}

function test_deploy {
	put ebizzy  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
        assert_define BENCHMARK_EBIZZY_CHUNKS
        assert_define BENCHMARK_EBIZZY_CHUNK_SIZE
        assert_define BENCHMARK_EBIZZY_TIME
        assert_define BENCHMARK_EBIZZY_THREADS

	report "cd $JTA_HOME/jta.$TESTDIR; ./ebizzy -m -n $BENCHMARK_EBIZZY_CHUNKS -P -R -s $BENCHMARK_EBIZZY_CHUNK_SIZE  -S $BENCHMARK_EBIZZY_TIME -t $BENCHMARK_EBIZZY_THREADS"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
