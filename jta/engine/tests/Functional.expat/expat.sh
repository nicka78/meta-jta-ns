tarball=expat-2.0.0.tar.gz

function test_build {
    # This patch adds pass/fail output for tests.
    cd tests
    patch -p0 < $TEST_HOME/xmltest.sh.patch
    cd -

    # XML Test Suite
    mkdir -p XML-Test-Suite

    # Possible defect - hardcoded xmlts name
    tar zxf $TEST_HOME/xmlts20080827.tar.gz -C XML-Test-Suite

    CXXFLAGS='-I. -I./lib -g -O2'
    ./configure --build=`uname -m`-gnu-linux --host="$PREFIX" #CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" 
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" 1>/dev/null
    make CFLAGS="$CFLAGS" CC="$CC" CXX=$PREFIX-g++ CXX="$CXX"  CXXFLAGS="$CXXFLAGS" tests/runtestspp; 1>/dev/null && touch test_suite_ready || exit 1
}

function test_deploy {
    tar cf XML-Test-Suite.tar XML-Test-Suite/
    put -r XML-Test-Suite.tar tests/.libs/* tests/xmltest.sh  $JTA_HOME/jta.$TESTDIR/;

    cmd "cd $JTA_HOME/jta.$TESTDIR; tar xf XML-Test-Suite.tar"
    cmd "mkdir -p $JTA_HOME/xmlwf";

    put xmlwf/.libs/xmlwf  $JTA_HOME/xmlwf/xmlwf;
}

function test_run {
    report "cd $JTA_HOME/jta.$TESTDIR; ./runtestspp 2>&1 | tr ',' '\n'| sed s/^\ //"  
    report_append "cd $JTA_HOME/jta.$TESTDIR; ./xmltest.sh"  
}

function test_processing {
    assert_define EXPAT_SUBTEST_COUNT_POS
    assert_define EXPAT_SUBTEST_COUNT_NEG
    
    log_compare "$TESTDIR" $EXPAT_SUBTEST_COUNT_POS "100%: Checks: 48|passed" "p"
    log_compare "$TESTDIR" $EXPAT_SUBTEST_COUNT_NEG "failed" "n"
}


. $JTA_ENGINE_PATH/scripts/functional.sh
