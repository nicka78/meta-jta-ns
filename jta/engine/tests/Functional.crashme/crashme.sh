tarball=crashme_2.4.tar.bz2

function test_build {
    patch -p1 -N -s < $TEST_HOME/crashme_2.4-9.patch
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || exit 1
}

function test_deploy {
	put crashme  $JTA_HOME/jta.$TESTDIR/
}

function test_run {   
	assert_define FUNCTIONAL_CRASHME_NBYTES
	assert_define FUNCTIONAL_CRASHME_INC
	assert_define FUNCTIONAL_CRASHME_SRAND
	assert_define FUNCTIONAL_CRASHME_NTRYS
	assert_define FUNCTIONAL_CRASHME_NSUB

	report "cd $JTA_HOME/jta.$TESTDIR; ./crashme $FUNCTIONAL_CRASHME_NBYTES.$FUNCTIONAL_CRASHME_INC $FUNCTIONAL_CRASHME_SRAND $FUNCTIONAL_CRASHME_NTRYS $FUNCTIONAL_CRASHME_NSUB 2"  
}

function test_processing {   
	log_compare "$TESTDIR" "1" "0 ...  3000" "p"
}

. $JTA_ENGINE_PATH/scripts/functional.sh
