tarball=synctest.tar.gz

function test_build {
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || exit 1
}

function test_deploy {
	put synctest  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    assert_define FUNCTIONAL_SYNCTEST_MOUNT_BLOCKDEV
    assert_define FUNCTIONAL_SYNCTEST_MOUNT_POINT
    assert_define FUNCTIONAL_SYNCTEST_LEN
    assert_define FUNCTIONAL_SYNCTEST_LOOP
    
    hd_test_mount_prepare $FUNCTIONAL_SYNCTEST_MOUNT_BLOCKDEV $FUNCTIONAL_SYNCTEST_MOUNT_POINT
    report "cd $FUNCTIONAL_SYNCTEST_MOUNT_POINT/jta.$TESTDIR; $JTA_HOME/jta.$TESTDIR/synctest $FUNCTIONAL_SYNCTEST_LEN $FUNCTIONAL_SYNCTEST_LOOP"

    hd_test_clean_umount $FUNCTIONAL_SYNCTEST_MOUNT_BLOCKDEV $FUNCTIONAL_SYNCTEST_MOUNT_POINT
}

function test_processing {
	log_compare "$TESTDIR" "1" "PASS : sync interrupted" "p"
}

. $JTA_ENGINE_PATH/scripts/functional.sh

