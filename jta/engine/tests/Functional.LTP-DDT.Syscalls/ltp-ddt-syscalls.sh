source $JTA_ENGINE_PATH/scripts/overlays.sh
set_overlay_vars

source $JTA_ENGINE_PATH/scripts/reports.sh
source $JTA_ENGINE_PATH/scripts/functions.sh

source $TEST_HOME/../LTP-DDT/ltp-ddt.sh

function test_run {
    report "cd $JTA_HOME/jta.$TESTDIR/testscripts; ./syscalls.sh"  
}

function test_build {
    report "echo in test_build"
}


function test_processing {
    P_CRIT="TPASS"
    N_CRIT="TFAIL"
    log_compare "$TESTDIR" "3897" "${P_CRIT}" "p"
    log_compare "$TESTDIR" "13" "${N_CRIT}" "n"
}

test_run 
get_testlog $TESTDIR
test_processing
