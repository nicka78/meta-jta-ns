tarball=hackbench.tar.gz

function test_build {
    $CC -lpthread hackbench.c -o hackbench && touch test_suite_ready || exit 1
}

function test_deploy {
	put hackbench  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR; ./hackbench $groups"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh