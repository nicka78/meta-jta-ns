tarball=blobsallad-src.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/blobsallad.Makefile.patch
    patch -p0 -N -s < $TEST_HOME/blobsallad.auto.patch
    patch -p0 -N -s < $TEST_HOME/bs_main.c.patch

    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" SDKROOT="$SDKROOT"  && touch test_suite_ready || exit 1
}

function test_deploy {
	put blobsallad  $JTA_HOME/jta.$TESTDIR/
	put -r maps  $JTA_HOME/jta.$TESTDIR/ 
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR; export DISPLAY=:0; xrandr | awk '/\*/ {split(\$1, a, \"x\"); exit(system(\"./blobsallad \" a[1]  a[2]))}'"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
