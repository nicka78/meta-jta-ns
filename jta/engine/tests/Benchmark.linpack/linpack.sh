tarball=linpack.tar.bz2

function test_build {
    patch -p0 -N -s < $TEST_HOME/linpack.c.patch
    $CC $CFLAGS -O -lm -o linpack linpack.c && touch test_suite_ready || exit 1
}

function test_deploy {
	put linpack  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR && ./linpack"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh