tarball=aiostress.tar.gz

function test_build {
    $CC -I $SDKROOT/usr/include -L $SDKROOT/usr/lib  -Wall -lpthread -laio aiostress.c -o aiostress && touch test_suite_ready || exit 1 
}

function test_deploy {
	put aiostress  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
    assert_define FUNCTIONAL_AIOSTRESS_MOUNT_BLOCKDEV
    assert_define FUNCTIONAL_AIOSTRESS_MOUNT_POINT
    assert_define FUNCTIONAL_AIOSTRESS_SIZE
    
    hd_test_mount_prepare $FUNCTIONAL_AIOSTRESS_MOUNT_BLOCKDEV $FUNCTIONAL_AIOSTRESS_MOUNT_POINT
    report "cd $FUNCTIONAL_AIOSTRESS_MOUNT_POINT/jta.$TESTDIR; $JTA_HOME/jta.$TESTDIR/aiostress -s $FUNCTIONAL_AIOSTRESS_SIZE ./testfile"
    
    hd_test_clean_umount $FUNCTIONAL_AIOSTRESS_MOUNT_BLOCKDEV $FUNCTIONAL_AIOSTRESS_MOUNT_POINT
}

function test_processing {
	true
}

. $JTA_ENGINE_PATH/scripts/functional.sh

