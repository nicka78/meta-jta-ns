source ../tests/OpenSSL/openssl.sh

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR; bash run-tests.sh"  
}

function test_processing {
	P_CRIT="passed|ok"
	N_CRIT="skip"

	log_compare "$TESTDIR" "176" "${P_CRIT}" "p"
	log_compare "$TESTDIR" "86" "${N_CRIT}" "n"
}

test_run
get_testlog $TESTDIR
test_processing

