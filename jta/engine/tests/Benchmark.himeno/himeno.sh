tarball=himeno.tar.bz2

function test_build {
    CFLAGS+=" -O3"  
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" CFLAGS="$CFLAGS" && touch test_suite_ready || exit 1
}

function test_deploy {
	put bmt  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR && ./bmt"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
