tarball=ltp-ddt.tar.gz

# source $JTA_ENGINE_PATH/scripts/overlays.sh
# set_overlay_vars

# source $JTA_ENGINE_PATH/scripts/reports.sh
# source $JTA_ENGINE_PATH/scripts/functions.sh


TESTDIR="LTP-DDT"
TEST_HOME="$WORKSPACE/../tests/LTP-DDT"
TRIPLET=$TESTDIR-$PLATFORM

pre_test $TESTDIR

if $Rebuild; then
if pre_build; then
  make autotools
  ./configure CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS="$LDFLAGS" --with-open-posix-testsuite --without-perl --without-python --target=$PREFIX --host=$PREFIX --prefix=`pwd`/target_bin --build=`uname -m`-unknown-linux-gnu PLATFORM=beaglebone
    # ./configure CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS=-L${SDKROOT}/usr/lib --with-open-posix-testsuite --without-perl --without-python --target=$PREFIX --host=$PREFIX --prefix=`pwd`/target_bin --build=`uname -m`-unknown-linux-gnu PLATFORM=beaglebone
  make SKIP_IDCHECK=1  KERNEL_INC=. KERNEL_USR_INC=. -j1 all
    # make CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS="-L. -L${SDKROOT}/usr/lib" SKIP_IDCHECK=1 PLATFORM=beaglebone KERNEL_INC=. KERNEL_USR_INC=. -j16 all
  make SKIP_IDCHECK=0 PLATFORM=beaglebone install

  cd target_bin

  touch ../test_suite_ready
fi
post_build
fi

pre_deploy
# DEPLOY TO TARGET
  put -r target_bin/*  $JTA_HOME/jta.$TESTDIR/
post_deploy



