tarball=NetPIPE-3.7.1.tar.gz

function test_build {
    patch -p1 -N -s < ../../tarballs/netpipe-makefile.patch
    make CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC="$CC" LD="$LD" && touch test_suite_ready || exit 1
}

function test_deploy {
	put NPtcp  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	cmd "killall -SIGKILL NPtcp 2>/dev/null; exit 0"

	# Start netpipe server on Jenkins host
	netpipe_exec=`which NPtcp`

	if [ -z $netpipe_exec ];
	then 
	 echo "ERROR: Cannot find netpipe"
	 false
	else
	 $netpipe_exec -p 2 &
	fi

        assert_define BENCHMARK_NETPIPE_PERT

	if [ "$BENCHMARK_NETPIPE_SRV" = "default" ]; then
	  srv=$SRV_IP
	else
	  srv=$BENCHMARK_NETPIPE_SRV
	fi

	report "cd $JTA_HOME/jta.$TESTDIR; ./NPtcp -h $srv -p $BENCHMARK_NETPIPE_PERT" $JTA_HOME/jta.$TESTDIR/${TESTDIR}.log
}

source $JTA_ENGINE_PATH/scripts/functions.sh

source $JTA_ENGINE_PATH/scripts/overlays.sh
set_overlay_vars


pre_test $TESTDIR

if $Rebuild; then
    build
fi

deploy

test_run


