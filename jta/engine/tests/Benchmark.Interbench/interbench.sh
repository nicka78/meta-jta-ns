tarball=interbench-0.31.tar.bz2

function test_build {
    patch -p0 < $TEST_HOME/interbench.c.patch
    make CC="$CC" AR="$AR" RANLIB="$RANLIB" CXX="$CXX" CPP="$CPP" CXXCPP="$CXXCPP" && touch test_suite_ready || exit 1
}

function test_deploy {
	put interbench  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR; ./interbench -L 1 || ./interbench -L 1"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
