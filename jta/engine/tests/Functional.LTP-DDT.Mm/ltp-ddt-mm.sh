source $JTA_ENGINE_PATH/scripts/overlays.sh
set_overlay_vars

source $JTA_ENGINE_PATH/scripts/reports.sh
source $JTA_ENGINE_PATH/scripts/functions.sh

source $TEST_HOME/../LTP-DDT/ltp-ddt.sh

function test_run {
    report "cd $JTA_HOME/jta.$TESTDIR/testscripts; ./mm.sh"  
}

function test_processing {
    P_CRIT="PASS"
    log_compare "$TESTDIR" "937" "${P_CRIT}" "p"
}

test_run
get_testlog $TESTDIR
test_processing
