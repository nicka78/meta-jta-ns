#!/bin/python
# Parameters for this script are: JOB_NAME, PLATFORM, TARBALL_TEMPLATE, BUILD_ID, BUILD_NUMBER, Number of last builds to plot  

import os, re, sys

sys.path.insert(0, os.environ['JTA_PARSER_PATH'])
import common as plib

ref_section_pat = "^\[[\d\w_.-]+.[gle]{2}\]"
cur_search_pat = re.compile("^(md5|aes-192 cbc|sha512)(\s*)([\d.k]{6,10})(\s*)([\d.k]{6,10})(\s*)([\d.k]{6,10})(\s*)([\d.k]{6,10})(\s*)([\d.k]{6,10})",re.MULTILINE)

cur_dict = {}

pat_result = plib.parse(cur_search_pat)
if pat_result:
	for i in range(3):
		testname = pat_result[i][0]
		if " " in testname:
			testname = pat_result[i][0].split(" ")[0] + "_" + pat_result[i][0].split(" ")[1]

		cur_dict[testname+".16bytes"] = pat_result[i][2].rstrip("k")
		cur_dict[testname+".64bytes"] = pat_result[i][4].rstrip("k")
		cur_dict[testname+".256bytes"] = pat_result[i][6].rstrip("k")
		cur_dict[testname+".1024bytes"] = pat_result[i][8].rstrip("k")
		cur_dict[testname+".8192bytes"] = pat_result[i][10].rstrip("k")

sys.exit(plib.process_data(ref_section_pat, cur_dict, 'm', 'Rate,1000s of bytes/second'))
