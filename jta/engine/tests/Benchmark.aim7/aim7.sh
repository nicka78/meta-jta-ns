tarball=osdl-aim-7.0.1.13.tar.gz

function test_build {
        ./bootstrap
        PKG_CONFIG_PATH=${SDKROOT}/usr/lib/pkgconfig PKG_CONFIG_ALLOW_SYSTEM_LIBS=1 PKG_CONFIG_SYSROOT_DIR=${SDKROOT} ./configure --host=$HOST --build=`uname -m`-linux-gnu LDFLAGS=-L${SDKROOT}/usr/lib CPPFLAGS=-I${SDKROOT}/usr/include  CFLAGS=-I${SDKROOT}/usr/include LIBS=-laio --prefix=$JTA_HOME/$TESTDIR --datarootdir=$JTA_HOME/$TESTDIR
        make && touch test_suite_ready || exit 1
}

function test_deploy {
	put src/reaim  $JTA_HOME/jta.$TESTDIR/
	put -r data  $JTA_HOME/jta.$TESTDIR/
	put -r scripts  $JTA_HOME/jta.$TESTDIR/
}

function test_run {
	report "cd $JTA_HOME/jta.$TESTDIR; mkdir /tmp/diskdir; ./reaim -c ./data/reaim.config -f ./data/workfile.short"  
	report_append "cd $JTA_HOME/jta.$TESTDIR; ./reaim -c ./data/reaim.config -f ./data/workfile.all_utime"  
}

. $JTA_ENGINE_PATH/scripts/benchmark.sh
