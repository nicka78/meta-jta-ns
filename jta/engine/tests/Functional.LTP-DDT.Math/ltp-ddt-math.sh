source $JTA_ENGINE_PATH/scripts/overlays.sh
set_overlay_vars

source $JTA_ENGINE_PATH/scripts/reports.sh
source $JTA_ENGINE_PATH/scripts/functions.sh

source $TEST_HOME/../LTP-DDT/ltp-ddt.sh

function test_run {
    report "cd $JTA_HOME/jta.$TESTDIR/testscripts; ./math.sh"  
}

function test_processing {
    P_CRIT="TPASS"
    N_CRIT="TFAIL"

    log_compare "$TESTDIR" "22" "${P_CRIT}" "p"
    log_compare "$TESTDIR" "0" "${N_CRIT}" "n"
}

test_run
get_testlog $TESTDIR
test_processing
