tarball=ltp_2011-09-07.tar.bz2

# source $JTA_ENGINE_PATH/scripts/functions.sh
# source $JTA_ENGINE_PATH/scripts/overlays.sh
# source $JTA_ENGINE_PATH/scripts/reports.sh
# set_overlay_vars


TESTDIR="LTP"
TEST_HOME="$WORKSPACE/../tests/LTP"
TRIPLET=$TESTDIR-$PLATFORM

function test_build {
    make autotools
    ./configure CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS="$LDFLAGS" --with-open-posix-testsuite --without-perl --without-python --target=$PREFIX --host=$PREFIX --prefix=`pwd`/target_bin --build=`uname -m`-unknown-linux-gnu
        # ./configure CC="${CC}" AR="${AR}" RANLIB="${RANLIB}" LDFLAGS=-L${SDKROOT}/usr/lib --with-open-posix-testsuite --without-perl --without-python --target=$PREFIX --host=$PREFIX --prefix=`pwd`/target_bin --build=`uname -m`-unknown-linux-gnu
    make CC="${CC}"
    make install
  
    cd target_bin
  
    # --- begin Filesystem tweaks ---
    grep -Ev 'growfiles|rwtest|iogen|quota' runtest/fs | tee runtest/fs_jta
  
    # We do not have enough free space on XXX for default configuration.
    # Adjust to 25 (this test) + 20*3 (fs_di) + 10 (scripts and binaries) MiB.
    # (-S 40 enables frag test in fs_di; the value should be twice as large as the one used for -s.)
    sed -i '/lftest /clftest01        lftest 25' runtest/fs_jta
    sed -i '/fs_di fs_di/cfs_di fs_di -d $TMPDIR -l 2 -s 20 -S 40' runtest/fs_jta
  
    echo "fsstress fsstress -d . -l 100 -n 40 -S" >> runtest/fs_jta
    # --- end Filesystem tweaks ---
  
    # --- begin OpenPOSIX tweaks ---
    sed -i.bak "27 a pkill -9 -f run-test" bin/run-posix-option-group-test.sh
    sed -i.bak "54 a echo \"\$testname: execution: PASS\"" bin/run-tests.sh
  
    # Thread test groups is disabled
    sed -i.bak s/THR\ // bin/run-all-posix-option-group-tests.sh
    rm -f conformance/interfaces/aio_cancel/3-1.run-test
    rm -f conformance/interfaces/aio_cancel/4-1.run-test
    rm -f conformance/interfaces/aio_cancel/6-1.run-test
    rm -f conformance/interfaces/aio_cancel/7-1.run-test
    rm -f conformance/interfaces/aio_error/2-1.run-test
    rm -f conformance/interfaces/shm_open/23-1.run-test
    rm -f conformance/interfaces/lio_listio/2-1.run-test
    rm -f conformance/interfaces/timer_settime/5-2.run-test
    rm -f conformance/interfaces/mq_timedreceive/5-3.run-test
    rm -f conformance/interfaces/mq_timedsend/12-1.run-test
  #  rm -f conformance/interfaces/mq_receive/13-1.run-test
  
    # XXX and XXX test sets are different
    if [ "$PLATFORM" = "ia32" ]; then
      rm -f conformance/interfaces/aio_fsync/*.run-test
      rm -f conformance/interfaces/mq_receive/13-1.run-test
    else
      rm -f conformance/interfaces/aio_fsync/5-1.run-test
      rm -f conformance/interfaces/aio_fsync/8-4.run-test
   fi;
    # --- end OpenPOSIX tweaks ---
  
    touch ../test_suite_ready
}

function test_deploy {
    # RTC Device
    if [ ! -e testcases/kernel/device-drivers/rtc/test_suite_ready ]; then
      cd testcases/kernel/device-drivers/rtc
      make CC="$CC" && touch test_suite_ready || exit 1
      cd -;
    fi;

    # TARGET PREP.
      safe_cmd "mkdir -p jta.$TESTDIR/devices && mkdir -p jta.$TESTDIR/tools && mkdir -p jta.$TESTDIR/tools/apicmds"

    # DEPLOY TO TARGET
    if [ "${JOB_NAME/*.}" = "Devices" ]; then
      put -r testcases/kernel/device-drivers/rtc/rtc-test  $JTA_HOME/jta.$TESTDIR/

    elif [ "${JOB_NAME/*.}" = "Filesystem" ]; then
      mkdir -p fs_tests
      # Corresponding build script hard-codes the amount of required free space on a target into runtest/fs_jta.
      # Parse runtest/fs_jta to copy only the required tests because the whole set is too large for XXX
      awk '/^[^#]/ { print $2 }' target_bin/runtest/fs_jta > fs_tests.include
      rsync -arhlP --include-from=fs_tests.include --include='testcases/bin/tst*' --include='create_datafile' --include='frag' --include='testcases/bin/fs*' --exclude='testcases/bin/*' --exclude=conformance target_bin/ fs_tests/
      put -r fs_tests/*  $JTA_HOME/jta.$TESTDIR/

    elif [ "${JOB_NAME/*.}" = "Open_Posix" ]; then
      put -r target_bin  /tmp/jta.$TESTDIR/
    fi
}

pre_test $TESTDIR

if $Rebuild; then
    build
fi

deploy
