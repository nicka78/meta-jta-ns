{
    "testName": "Benchmark.bc",
    "specs": 
    [
        {
            "name":"bc-exp1",
            "EXPR1":"2*2",
            "EXPR2":"3*3"
        },
        {
            "name":"bc-exp2",
            "EXPR1":"2+2",
            "EXPR2":"3+3"
        },
        {
            "name":"default",
            "EXPR1":"2+2",
            "EXPR2":"3+3"
        }
    ]
}

